/********
 * 
 * Nach einem erfolgreichen HTTP-GET-Aufruf von URL -(bitte sehen .env file)-
 * liest dieses Programm pro Entität die folgenden Daten sendet die Ausgabe an die Konsole an:
 * 
 * -    id
 * -    name
 * -    properties/createdService und
 * -    Sämtliche Attribute unterhalb idpath
 *
 * Dieses Programm verwendet Axios(), einen HTTP-Client, 
 * um die Daten vom Webservice abzurufen.
 * 
 * Die Webserver-URL ist in der Environment Variable File (.env) versteckt.
 * 
 * Weitere Verbesserungen wären das Rendern der Daten in Front-End-Libraries wie React.js anstelle der Konsole.
 * 
 * 
 * Author: Laz Bohale
 * Date: 02.05.2021
 *******/
 const axios = require('axios');
 require ('dotenv').config(); 
 
 //get data from the webservice
 axios.get(process.env.URL)    //inject URL from environment variables file
       .then((response) => {
 
         //HTTP GET returned 200 status
         response.data.response.entities.forEach(element => {
 
           //display Entity ID, Name and Properties
           console.clear();  
           console.log("\n********************************************\n");
           console.log("Entity ID :", element.id);
           console.log("Entity Name :", element.name);
           console.log("Properties/createdService :", element.properties['createdService']);
 
           //show Entity Attribute Names 
           Object.keys(element.data.attributes).map(attribute => {
              console.log("*\nAttribute Name :", attribute); 
 
              //show Entity Attribute Values
              console.log("Values :");  
              element.data.attributes[attribute].values.forEach(val => {
               Object.entries(val).forEach(([key, value]) => {
                 //nested attribute ? 
                 if (key == 'properties') {
                     console.log("Properties")
                   Object.entries(val.properties).forEach(([key1, value1]) => {
                     console.log(`        ${key1}: ${value1}`);
                   })
                 } else {
                   console.log(`     ${key}: ${value}`);
                 }
               });
              })
           });
         });
         console.log("\n********************************************\n");
 
       })
       .catch((error) => { console.log(error); });  //webservice returned an error
 