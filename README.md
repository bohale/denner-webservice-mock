# Node-js Axios Application
#
# Ein JavaScript-Programm, um Webserver-Daten über HTTPS abzurufen und dann auf der Konsole anzuzeigen. 
# In den Programmkommentaren finden Sie eine detaillierte Beschreibung der Logik.
 

## Führen Sie den Code lokal aus

Stellen Sie sicher, dass Sie über die neuesten [Node.js] verfügen (http://nodejs.org/).

https://gitlab.com/bohale/denner-webservice-mock.git

Zum Ausführen 
1) https://gitlab.com/bohale/denner-webservice-mock.git klonen und 
2) npm install